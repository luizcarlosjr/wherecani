package com.example.wherecani;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.example.wherecani.dao.EventoDAO;
import com.example.wherecani.dao.UsuarioDAO;
import com.example.wherecani.model.Evento;
import com.example.wherecani.model.Usuario;

public class CadastroEvento extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cadastro_evento);

        Intent intent = getIntent();
        final boolean isEdit = intent.getExtras().getBoolean("isEdit", false);

        final Button cadCadastrar = (Button) findViewById(R.id.cadCadastrar);
        final Button cadok = (Button) findViewById(R.id.cadok);

        final EditText nome = (EditText) findViewById(R.id.cadNome);
        final EditText data = (EditText) findViewById(R.id.cadData);
        final EditText valor = (EditText) findViewById(R.id.cadValor);
        final EditText local = (EditText) findViewById(R.id.cadLocal);
        final EditText pais = (EditText) findViewById(R.id.cadPais);
        final EditText estado = (EditText) findViewById(R.id.cadEstado);
        final EditText cidade = (EditText) findViewById(R.id.cadCidade);
        final EditText bairro = (EditText) findViewById(R.id.cadBairro);

        final EventoDAO dao = new EventoDAO(CadastroEvento.this);

        if(isEdit) {

            final int position = intent.getExtras().getInt("position");

            System.out.println("ID AQUI ------> " + position);

            final List<Evento> eventos = dao.buscaEventos();

            final Evento ev = eventos.get(position);

            nome.setEnabled(false);
            data.setEnabled(false);
            valor.setEnabled(false);
            pais.setEnabled(false);
            estado.setEnabled(false);
            cidade.setEnabled(false);
            bairro.setEnabled(false);
            local.setEnabled(false);

            TextView title = (TextView) findViewById(R.id.cadTituloEv);
            TextView cadNome = (TextView) findViewById(R.id.cadNomeText);
            title.setText(ev.getNome());
            title.setTextSize(30);

            cadNome.setHeight(0);
            cadNome.setVisibility(View.INVISIBLE);

            nome.setVisibility(View.INVISIBLE);
            nome.setHeight(0);

            String dataEvento = ev.getDataEvento();
            data.setText(dataEvento.substring(0, 2) + "/" + dataEvento.substring(2, 4) + "/" + dataEvento.substring(4, 8));
            valor.setText(ev.getValor().toString());
            local.setText(ev.getEndereco());
            pais.setText(ev.getPais());
            estado.setText(ev.getEstado());
            cidade.setText(ev.getCidade());
            bairro.setText(ev.getBairro());

            final String user = intent.getExtras().getString("user");
            final Long senha = intent.getExtras().getLong("senha");
            final UsuarioDAO usuarioDAO = new UsuarioDAO(CadastroEvento.this);
            final Usuario usuario = usuarioDAO.recuperarUsuario(user, senha);


            if(!usuario.getIdEventos().contains(position)) {

                cadCadastrar.setText("Confirmar presença");
                cadok.setVisibility(View.INVISIBLE);

                cadCadastrar.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        usuario.setIdEventos(
                                usuario.getIdEventosString() + "," + position
                        );

                        cadCadastrar.setVisibility(View.INVISIBLE);
                        cadCadastrar.setHeight(0);
                        cadok.setText("Aguardando validação");
                        cadok.setEnabled(false);
                        cadok.setVisibility(View.VISIBLE);

                        if(usuario.getId() != 1L) {

                           Double d = new Double(ev.getValor() * 0.25);
                           Long pt = d.longValue();

                           usuario.setPontos(
                                   usuario.getPontos() + pt
                           );
                        }

                        usuarioDAO.altera(usuario);

                    }

                });

            } else {
                cadCadastrar.setVisibility(View.INVISIBLE);
                cadCadastrar.setHeight(0);
                cadok.setText("Presença confirmada");
                cadok.setEnabled(false);
            }

        } else {

            cadok.setVisibility(View.INVISIBLE);
            cadok.setEnabled(false);

            cadCadastrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    SimpleDateFormat formataData = new SimpleDateFormat("dd/MM/yyyy");
                    Date dataCriacao = new Date();
                    String dataFormatada = formataData.format(dataCriacao);
                    System.out.println("Data formatada: " + dataFormatada);

                    Intent intent = getIntent();
                    final String usuarioCriacao = intent.getExtras().getString("usuarioCriacao");

                    Evento Evento = new Evento();
                    Evento.setDataCriacao(dataFormatada);
                    Evento.setNome(nome.getText().toString());
                    Evento.setUsuarioCriacao(usuarioCriacao);
                    Evento.setEndereco(local.getText().toString());
                    Evento.setDataEvento(data.getText().toString());
                    Evento.setValor(Double.parseDouble(valor.getText().toString()));
                    Evento.setPais(pais.getText().toString());
                    Evento.setEstado(estado.getText().toString());
                    Evento.setCidade(cidade.getText().toString());
                    Evento.setBairro(bairro.getText().toString());

                    if(Evento.getDataEvento().length() != 8) {
                        Toast.makeText(CadastroEvento.this, "Data inválida.", Toast.LENGTH_LONG).show();
                    } else {
                        dao.novoEvento(Evento);

                        Intent intentVaiPraPrincipal = new Intent(CadastroEvento.this, ListaEventos.class);
                        intentVaiPraPrincipal.putExtra("login", usuarioCriacao);
                        intentVaiPraPrincipal.putExtra("senha", intent.getExtras().getLong("senha"));
                        intentVaiPraPrincipal.putExtra("pontos",intent.getExtras().getString("pontos"));
                        startActivity(intentVaiPraPrincipal);
                    }
                }
            });

        }
    }
}


