package com.example.wherecani;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.wherecani.dao.UsuarioDAO;
import com.example.wherecani.model.Usuario;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setLogo(R.mipmap.ic_wherecani);
        getSupportActionBar().setDisplayUseLogoEnabled(true);

        Button login = (Button) findViewById(R.id.login);


        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText user = (EditText) findViewById(R.id.user);
                EditText senha = (EditText) findViewById(R.id.password);

                // verifica usuario e senha nulos
                if(user.getText().toString().equals("") ||
                    senha.getText().toString().equals("")) {
                    Toast.makeText(MainActivity.this, "Entre com usuário e senha!", Toast.LENGTH_SHORT).show();
                } else {
                    UsuarioDAO dao =  new UsuarioDAO(MainActivity.this);

                    System.out.println("usuario view - " + user.getText().toString() + " com senha view - " + senha.getText().toString());

                    Usuario usuario = dao.recuperarUsuario(user.getText().toString(), Long.parseLong(senha.getText().toString()));

                    if(usuario.getId() != null) {
                        Toast.makeText(MainActivity.this, "Bem vindo " + usuario.getNome() + "!", Toast.LENGTH_SHORT).show();

                        Intent intentVaiPraLista = new Intent(MainActivity.this, ListaEventos.class);
                        intentVaiPraLista.putExtra("nome", usuario.getNome());
                        intentVaiPraLista.putExtra("pontos", usuario.getPontosFormatter());
                        intentVaiPraLista.putExtra("login", usuario.getUser());
                        intentVaiPraLista.putExtra("senha", usuario.getSenha());
                        startActivity(intentVaiPraLista);
                    } else {
                        Toast.makeText(MainActivity.this, "Usuario Não encontrado!", Toast.LENGTH_SHORT).show();
                    }
                }



                //Intent intentVaiProFormulario = new Intent(ListaAlunosActivity.this, FormularioActivity.class);
                //startActivity(intentVaiProFormulario);
            }
        });

        Button cadastre = (Button) findViewById(R.id.cadastre);

        cadastre.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentVaiPraCadastro = new Intent(MainActivity.this, CadastroUsuario.class);
                intentVaiPraCadastro.putExtra("isEdit", false);
                startActivity(intentVaiPraCadastro);
            }
        });
    }
}
