package com.example.wherecani.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.example.wherecani.model.Evento;

import java.util.ArrayList;
import java.util.List;

public class EventoDAO extends SQLiteOpenHelper {

    private static final int DB_VERSION = 1;

    public EventoDAO(Context context) {
        super(context, "evento", null, EventoDAO.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        System.out.println("CREATE EVENTO");
        String sql =
                "CREATE TABLE Eventos (" +
                        "id INTEGER PRIMARY KEY, " +
                        "usuario_criacao TEXT NOT NULL, " +
                        "data_criacao TEXT NOT NULL," +
                        "data_evento TEXT NOT NULL," +
                        "nome TEXT NOT NULL," +
                        "valor NUMERIC(7,2) NOT NULL," +
                        "endereco TEXT NOT NULL," +
                        "bairro TEXT," +
                        "cidade TEXT," +
                        "estado TEXT," +
                        "pais TEXT NOT NULL)";
        db.execSQL(sql);

        Evento eventoDefault1 = new Evento();
        Evento eventoDefault2 = new Evento();

        eventoDefault1.setPais("BR");
        eventoDefault1.setEstado("SP");
        eventoDefault1.setCidade("São Paulo");
        eventoDefault1.setBairro("Capital");
        eventoDefault1.setDataCriacao("25052019");
        eventoDefault1.setEndereco("(Excursão)São Paulo - SP");
        eventoDefault1.setValor(750.60);
        eventoDefault1.setDataEvento("13072019");
        eventoDefault1.setNome("A Hora do Trance – Além da imaginação");
        eventoDefault1.setId(1L);
        eventoDefault1.setUsuarioCriacao("root");

        this.eventoDefaultDB(eventoDefault1, db);

        eventoDefault2.setPais("BR");
        eventoDefault2.setEstado("RJ");
        eventoDefault2.setCidade("Rio de Janeiro");
        eventoDefault2.setBairro("Ilha de Guaratiba");
        eventoDefault2.setDataCriacao("25052019");
        eventoDefault2.setEndereco("Sítio Verde Morada, Estrada do Morgado 273 Ilha de Guaratiba, 22713-580");
        eventoDefault2.setValor(520.98);
        eventoDefault2.setDataEvento("13072019");
        eventoDefault2.setNome("TERRATRONIC Atlantis");
        eventoDefault2.setId(2L);
        eventoDefault2.setUsuarioCriacao("root");

        this.eventoDefaultDB(eventoDefault2, db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Eventos";
        db.execSQL(sql);
        onCreate(db);
    }

    public void novoEvento(Evento evento) {
        SQLiteDatabase db = getWritableDatabase();

        evento.setDataEvento(evento.getDataEvento().replace("/", ""));
        evento.setDataCriacao(evento.getDataCriacao().replace("/", ""));
        ContentValues dados = pegaDadosDoEvento(evento);

        db.insert("Eventos", null, dados);
    }

    private void eventoDefaultDB(Evento evento, SQLiteDatabase db) {

        //db = getWritableDatabase();

        ContentValues dados = pegaDadosDoEvento(evento);

        db.insert("Eventos", null, dados);
    }


    @NonNull
    private ContentValues pegaDadosDoEvento(Evento evento) {
        ContentValues dados = new ContentValues();
        dados.put("id", evento.getId());
        dados.put("usuario_criacao", evento.getUsuarioCriacao());
        dados.put("data_criacao", evento.getDataCriacao());
        dados.put("data_evento", evento.getDataEvento());
        dados.put("nome", evento.getNome());
        dados.put("valor", evento.getValor());
        dados.put("endereco", evento.getEndereco());
        dados.put("bairro", evento.getBairro());
        dados.put("cidade", evento.getCidade());
        dados.put("estado", evento.getEstado());
        dados.put("pais", evento.getPais());

        return dados;
    }


    public ArrayList<Evento> buscaEventos() {

        String sql = "SELECT * from Eventos;";

        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);


        ArrayList<Evento> eventos = new ArrayList<>();

        System.out.println("c.getCount() == " + c.getCount());
        if (c.getCount() > 0 && c.moveToFirst()) {
            do {
                System.out.println("c.moveToNext() == true");
                Evento evento = new Evento();

                evento.setId(c.getLong(c.getColumnIndex("id")));
                evento.setNome(c.getString(c.getColumnIndex("nome")));
                evento.setEndereco(c.getString(c.getColumnIndex("endereco")));
                evento.setDataCriacao(c.getString(c.getColumnIndex("data_criacao")));
                evento.setDataEvento(c.getString(c.getColumnIndex("data_evento")));
                evento.setValor(c.getDouble(c.getColumnIndex("valor")));
                evento.setUsuarioCriacao(c.getString(c.getColumnIndex("usuario_criacao")));
                evento.setBairro(c.getString(c.getColumnIndex("bairro")));
                evento.setCidade(c.getString(c.getColumnIndex("cidade")));
                evento.setEstado(c.getString(c.getColumnIndex("estado")));
                evento.setPais(c.getString(c.getColumnIndex("pais")));

                eventos.add(evento);
            } while (c.moveToNext());
        }
        c.close();
        return eventos;
    }

    public void altera(Evento evento) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues dados = pegaDadosDoEvento(evento);

        String[] params = {evento.getId().toString()};
        db.update("Eventos", dados, "id = ?", params);
    }
}
