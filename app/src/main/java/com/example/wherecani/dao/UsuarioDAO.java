package com.example.wherecani.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.NonNull;

import com.example.wherecani.model.Usuario;

public class UsuarioDAO extends SQLiteOpenHelper {

    private static final int DB_VERSION = 19;

    public UsuarioDAO(Context context) {
        super(context, "usuario", null, UsuarioDAO.DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        System.out.println("CREATE");
        String sql =
                "CREATE TABLE Usuario (" +
                        "id INTEGER PRIMARY KEY, " +
                        "user TEXT NOT NULL, " +
                        "password NUMERIC(8, 0) NOT NULL," +
                        "nome TEXT NOT NULL," +
                        "cpfCnpj NUMERIC(14, 0) NOT NULL, " +
                        "ddd NUMERIC(2, 0) NOT NULL," +
                        "telefone NUMERIC(9,0) NOT NULL," +
                        "email TEXT NOT NULL," +
                        "eventos TEXT NOT NULL," +
                        "data_nascimento TEXT NOT NULL," +
                        "pontos NUMERIC(7,0) NOT NULL," +
                        "bairro TEXT," +
                        "cidade TEXT," +
                        "estado TEXT," +
                        "pais TEXT NOT NULL)";
        db.execSQL(sql);

        Usuario usuarioDefault = new Usuario();

        usuarioDefault.setPais("BR");
        usuarioDefault.setEstado("RJ");
        usuarioDefault.setCidade("Rio");
        usuarioDefault.setBairro("Tijuca");
        usuarioDefault.setDataNascimento("01011998");
        usuarioDefault.setPontos(9999999L);
        usuarioDefault.setEmail("email@email.com");
        usuarioDefault.setIdEventos("0,1");
        usuarioDefault.setTelefone(99999999L);
        usuarioDefault.setDdd(99);
        usuarioDefault.setCpfCnpj(99999999999L);
        usuarioDefault.setNome("ADMIN");
        usuarioDefault.setUser("root");
        usuarioDefault.setSenha(123L);
        usuarioDefault.setId(1L);

        this.usuarioDefaultDB(usuarioDefault, db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String sql = "DROP TABLE IF EXISTS Usuario";
        db.execSQL(sql);
        onCreate(db);
    }

    public void novoUsuario(Usuario usuario) {
        SQLiteDatabase db = getWritableDatabase();
        usuario.setPontos(0L);

        /*Cursor c = db.rawQuery("SELECT MAX(id) FROM Usuario", null);
        long numUsuarios = 0;
        if (c.getCount() > 0 && c.moveToFirst()){
            numUsuarios = c.getLong(0);
            System.out.println(numUsuarios);
        }
        c.close();

        usuario.setId(numUsuarios + 1);*/ // Pega o maior id de usuário e incrementa. Aparentemente isso não é necessário. Verificar.

        usuario.setDataNascimento(usuario.getDataNascimento().replace("/", ""));
        usuario.setIdEventos("-2,-1");
        ContentValues dados = pegaDadosDoUsuario(usuario);

        db.insert("Usuario", null, dados);
    }

    private void usuarioDefaultDB(Usuario usuario, SQLiteDatabase db) {

        //db = getWritableDatabase();

        ContentValues dados = pegaDadosDoUsuario(usuario);

        db.insert("Usuario", null, dados);
    }


    @NonNull
    private ContentValues pegaDadosDoUsuario(Usuario usuario) {
        ContentValues dados = new ContentValues();
        dados.put("user", usuario.getUser());
        dados.put("password", usuario.getSenha());
        dados.put("nome", usuario.getNome());
        dados.put("cpfCnpj", usuario.getCpfCnpj());
        dados.put("ddd", usuario.getDdd());
        dados.put("telefone", usuario.getTelefone());
        dados.put("email", usuario.getEmail());
        dados.put("eventos", usuario.getIdEventosString());
        dados.put("data_nascimento", usuario.getDataNascimento());
        dados.put("pontos", usuario.getPontos());
        dados.put("bairro", usuario.getBairro());
        dados.put("cidade", usuario.getCidade());
        dados.put("estado", usuario.getEstado());
        dados.put("pais", usuario.getPais());
        return dados;
    }

    public Usuario recuperarUsuario(String login, Long senha) {
        String sql =
                "SELECT * from Usuario " +
                        "WHERE user LIKE '" + login + "' AND password = " + senha;
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);

        Usuario usuario = new Usuario();

        if(c.getCount() > 0 && c.moveToFirst()) {
            System.out.println(">> Chegou aqui == " + c.getCount());
            usuario.setId(c.getLong(c.getColumnIndex("id")));
            usuario.setSenha(c.getLong(c.getColumnIndex("password")));
            usuario.setUser(c.getString(c.getColumnIndex("user")));
            usuario.setNome(c.getString(c.getColumnIndex("nome")));
            usuario.setCpfCnpj(c.getLong(c.getColumnIndex("cpfCnpj")));
            usuario.setDdd(c.getInt(c.getColumnIndex("ddd")));
            usuario.setTelefone(c.getLong(c.getColumnIndex("telefone")));
            usuario.setEmail(c.getString(c.getColumnIndex("email")));
            usuario.setIdEventos(c.getString(c.getColumnIndex("eventos")));
            usuario.setDataNascimento(c.getString(c.getColumnIndex("data_nascimento")));
            usuario.setPontos(c.getLong(c.getColumnIndex("pontos")));
            usuario.setBairro(c.getString(c.getColumnIndex("bairro")));
            usuario.setCidade(c.getString(c.getColumnIndex("cidade")));
            usuario.setEstado(c.getString(c.getColumnIndex("estado")));
            usuario.setPais(c.getString(c.getColumnIndex("pais")));

            System.out.println("USER - " + usuario.getNome() + "COM SENHA - " + usuario.getSenha());

        }

        c.close();

        return usuario;
    }

    public void altera(Usuario usuario) {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues dados = pegaDadosDoUsuario(usuario);

        String[] params = {usuario.getId().toString()};
        db.update("Usuario", dados, "id = ?", params);
    }

    public boolean verifDispUsername (String username) {
        String sql =
                "SELECT user from Usuario " +
                        "WHERE user = '" + username + "'";
        SQLiteDatabase db = getReadableDatabase();
        Cursor c = db.rawQuery(sql, null);

        if(c.getCount() > 0) {
            c.close();
            return false;
        }
        c.close();
        return true;
    }
}
