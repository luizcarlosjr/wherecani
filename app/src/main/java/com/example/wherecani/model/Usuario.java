package com.example.wherecani.model;

import java.util.ArrayList;
import java.util.List;

public class Usuario {

    private Long id;
    private String user;
    private Long senha;
    private String nome;
    private Long cpfCnpj;
    private int ddd;
    private Long telefone;
    private String email;
    private String dataNascimento;
    private Long pontos;
    private String bairro;
    private String cidade;
    private String estado;
    private String pais;
    private List<Integer> idEventos;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public Long getSenha() {
        return senha;
    }

    public void setSenha(Long senha) {
        this.senha = senha;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Long getCpfCnpj() {
        return cpfCnpj;
    }

    public void setCpfCnpj(Long cpfCnpj) {
        this.cpfCnpj = cpfCnpj;
    }

    public int getDdd() {
        return ddd;
    }

    public void setDdd(int ddd) {
        this.ddd = ddd;
    }

    public Long getTelefone() {
        return telefone;
    }

    public void setTelefone(Long telefone) {
        this.telefone = telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {

        if(dataNascimento.length() == 8) {
            String data =
                    dataNascimento.substring(0, 2) +
                            "/" + dataNascimento.substring(2, 4) + "/" +
                            dataNascimento.substring(4);
            System.out.println(">>>  " + data + " >>> DTA Q VEM -- " + dataNascimento);
            this.dataNascimento = data;
        } else {
            this.dataNascimento = dataNascimento;
        }


    }

    public Long getPontos() {
        return pontos;
    }

    public String getPontosFormatter() {
       if(this.pontos >= 1000000) {
           return this.pontos.toString().substring(0, 1) +
                   "." + this.pontos.toString().substring(1, 4) + "." +
                   this.pontos.toString().substring(4, 7);
       } else if (this.pontos >= 1000) {
           return this.pontos.toString().substring(0, 1) +
                   "." + this.pontos. toString().substring(1, 4);
       } else {
           return this.pontos.toString();
       }
    }

    public void setPontos(Long pontos) {
        this.pontos = pontos;
    }

    public String getBairro() {
        return bairro;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public List<Integer> getIdEventos() {
        return idEventos;
    }

    public String getIdEventosString() {
        String s = "";

        for( int i : idEventos) {
            s += i;

            if(idEventos.indexOf(i) + 1 < idEventos.size()) {
                s += ",";
            }
        }

        System.out.println("==>>>> s -"+ s);
        return s;
    }

    public void setIdEventos(String ids) {

        String[] arrayIDS = ids.split(",");

        List<Integer> idsEv = new ArrayList<>();

        System.out.println("=====>>> ANTES DO FOR " + idsEv.size());

        for (String id : arrayIDS) {
            idsEv.add(Integer.parseInt(id));
            System.out.println("=====>>> NO FOR " + id);
        }

        System.out.println("=====>>> SET IDS " + arrayIDS.length);

        this.idEventos = idsEv;
    }
}
