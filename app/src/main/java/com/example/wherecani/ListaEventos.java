package com.example.wherecani;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.wherecani.dao.EventoDAO;
import com.example.wherecani.dao.UsuarioDAO;
import com.example.wherecani.model.Evento;
import com.example.wherecani.model.Usuario;

import java.util.ArrayList;

public class ListaEventos extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_lista_eventos);

        TextView pontos = (TextView) findViewById(R.id.pontos);
        TextView nomeLogado = (TextView) findViewById(R.id.nomeLogado);

        Intent intent = getIntent();
        final String nome = intent.getExtras().getString("nome");
        final String qtdPontos = intent.getExtras().getString("pontos");
        final String login = intent.getExtras().getString("login");
        final Long senha = intent.getExtras().getLong("senha");

        System.out.println(">>>" + nome + " > " + qtdPontos);
        pontos.setText("Pontos: " + qtdPontos);
        nomeLogado.setText(nome);

        EventoDAO dao = new EventoDAO(ListaEventos.this);
        ArrayList<Evento> eventos;
        RecyclerView rvEventos = (RecyclerView) findViewById(R.id.rvEventos);

        eventos = dao.buscaEventos();

        // System.out
        System.out.println("eventos == " + eventos);
        System.out.println("!!!!!!!!!!!!!!!!!! eventos.size() == " + eventos.size());
        for(int i = 0; i < eventos.size(); i++) {
            System.out.println("eventos.get(" + i + ") == " + eventos.get(i));
        }
        //

        EventosAdapter adapter = new EventosAdapter(eventos, this, login, senha);
        rvEventos.setAdapter(adapter);
        rvEventos.setLayoutManager(new LinearLayoutManager(this));


        Button cadastrar = (Button) findViewById(R.id.cadastrarEvento);

        cadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentVaiPraCadastroEvento = new Intent(ListaEventos.this, CadastroEvento.class);
                intentVaiPraCadastroEvento.putExtra("usuarioCriacao", login);
                intentVaiPraCadastroEvento.putExtra("senha", senha);
                intentVaiPraCadastroEvento.putExtra("pontos", qtdPontos);

                startActivity(intentVaiPraCadastroEvento);
            }
        });

        Button editUser = (Button) findViewById(R.id.editUser);

        editUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentVaiPraCadastroUsuario = new Intent(ListaEventos.this, CadastroUsuario.class);
                intentVaiPraCadastroUsuario.putExtra("isEdit", true);
                intentVaiPraCadastroUsuario.putExtra("login", login);
                intentVaiPraCadastroUsuario.putExtra("senha", senha);

                startActivity(intentVaiPraCadastroUsuario);
            }
        });

    }
    @Override
    protected void onResume() {

        super.onResume();

        Intent intent = getIntent();

        TextView pontos = (TextView) findViewById(R.id.pontos);

        final String qtdPontos = intent.getExtras().getString("pontos");
        final String login = intent.getExtras().getString("login");
        final Long senha = intent.getExtras().getLong("senha");

        final UsuarioDAO daoUser = new UsuarioDAO(ListaEventos.this);
        Usuario usuario = daoUser.recuperarUsuario(login, senha);

        pontos.setText("Pontos: " + usuario.getPontosFormatter());

    }
}
