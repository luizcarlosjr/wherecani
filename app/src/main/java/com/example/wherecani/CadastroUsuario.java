package com.example.wherecani;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.wherecani.dao.UsuarioDAO;
import com.example.wherecani.model.Usuario;

public class CadastroUsuario extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cadastro_usuario);

        Button cadCadastrar = (Button) findViewById(R.id.cadCadastrar);

        final EditText user = (EditText) findViewById(R.id.cadUser);
        final EditText senha = (EditText) findViewById(R.id.cadSenha);
        final EditText nome = (EditText) findViewById(R.id.cadNome);
        final EditText cpfCnpj = (EditText) findViewById(R.id.cadCpfCnpj);
        final EditText ddd = (EditText) findViewById(R.id.cadDdd);
        final EditText telefone = (EditText) findViewById(R.id.cadTelefone);
        final EditText email = (EditText) findViewById(R.id.cadEmail);
        final EditText dataNasc = (EditText) findViewById(R.id.cadDataNasc);
        final EditText pais = (EditText) findViewById(R.id.cadPais);
        final EditText estado = (EditText) findViewById(R.id.cadEstado);
        final EditText cidade = (EditText) findViewById(R.id.cadCidade);
        final EditText bairro = (EditText) findViewById(R.id.cadBairro);

        final UsuarioDAO dao = new UsuarioDAO(CadastroUsuario.this);

        Intent intent = getIntent();
        final boolean isEdit = intent.getExtras().getBoolean("isEdit", false);


        if (isEdit) {

            TextView title = (TextView) findViewById(R.id.cadTituloUser);
            title.setText("Editar");

            String login = intent.getExtras().getString("login");
            Long password = intent.getExtras().getLong("senha");

            final Usuario usuario = dao.recuperarUsuario(login, password);

            TextView subtitle = (TextView) findViewById(R.id.subtitle);
            subtitle.setText("Editando as credenciais do usuário " + usuario.getNome());

            user.setText(usuario.getUser());
            senha.setText(password.toString());
            nome.setText(usuario.getNome());
            cpfCnpj.setText(usuario.getCpfCnpj().toString());
            ddd.setText(String.valueOf(usuario.getDdd()));
            telefone.setText(usuario.getTelefone().toString());
            email.setText(usuario.getEmail());
            dataNasc.setText(usuario.getDataNascimento());
            pais.setText(usuario.getPais());
            estado.setText(usuario.getEstado());
            cidade.setText(usuario.getCidade());
            bairro.setText(usuario.getBairro());

            cadCadastrar.setText("Salvar");
            cadCadastrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    usuario.setUser(user.getText().toString());
                    usuario.setSenha(Long.parseLong(senha.getText().toString()));
                    usuario.setNome(nome.getText().toString());
                    usuario.setCpfCnpj(Long.parseLong(cpfCnpj.getText().toString()));
                    usuario.setDdd(Integer.parseInt(ddd.getText().toString()));
                    usuario.setTelefone(Long.parseLong(telefone.getText().toString()));
                    usuario.setEmail(email.getText().toString());
                    usuario.setDataNascimento(dataNasc.getText().toString());
                    usuario.setPais(pais.getText().toString());
                    usuario.setEstado(estado.getText().toString());
                    usuario.setCidade(cidade.getText().toString());
                    usuario.setBairro(bairro.getText().toString());

                    dao.altera(usuario);

                    Intent intentVaiPraLista = new Intent(CadastroUsuario.this, ListaEventos.class);

                    intentVaiPraLista.putExtra("nome", nome.getText().toString());
                    intentVaiPraLista.putExtra("pontos", usuario.getPontos());
                    intentVaiPraLista.putExtra("login", user.getText().toString());
                    intentVaiPraLista.putExtra("senha", Long.parseLong(senha.getText().toString()));

                    startActivity(intentVaiPraLista);

                }
            });

        } else {

            cadCadastrar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Usuario Usuario = new Usuario();

                    Usuario.setUser(user.getText().toString());
                    Usuario.setSenha(Long.parseLong(senha.getText().toString()));
                    Usuario.setNome(nome.getText().toString());
                    Usuario.setCpfCnpj(Long.parseLong(cpfCnpj.getText().toString()));
                    Usuario.setDdd(Integer.parseInt(ddd.getText().toString()));
                    Usuario.setTelefone(Long.parseLong(telefone.getText().toString()));
                    Usuario.setEmail(email.getText().toString());
                    Usuario.setDataNascimento(dataNasc.getText().toString());
                    Usuario.setPais(pais.getText().toString());
                    Usuario.setEstado(estado.getText().toString());
                    Usuario.setCidade(cidade.getText().toString());
                    Usuario.setBairro(bairro.getText().toString());

                    if (!dao.verifDispUsername(Usuario.getUser())) {
                        Toast.makeText(CadastroUsuario.this, "Nome de usuário indisponível.", Toast.LENGTH_LONG).show();
                    } else if (Usuario.getCpfCnpj().toString().length() != 11 && Usuario.getCpfCnpj().toString().length() != 14) {
                        Toast.makeText(CadastroUsuario.this, "CPF/CNPJ inválido.", Toast.LENGTH_LONG).show();
                    } else if (Usuario.getDataNascimento().length() != 10) {
                        Toast.makeText(CadastroUsuario.this, "Data de nascimento inválida.", Toast.LENGTH_LONG).show();
                    } else {
                        dao.novoUsuario(Usuario);

                        Intent intentVaiPraLogin = new Intent(CadastroUsuario.this, MainActivity.class);
                        startActivity(intentVaiPraLogin);
                    }
                }
            });

        }

    }

}
