package com.example.wherecani;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.wherecani.model.Evento;

import java.util.List;

public class EventosAdapter extends RecyclerView.Adapter<EventosAdapter.ViewHolder> {

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView nomeEvento;
        public TextView pontosEvento;
        public TextView valorEvento;
        public TextView enderecoEvento;
        public Button detalhes;

        public ViewHolder(View itemView) {
            super(itemView);

            nomeEvento = (TextView) itemView.findViewById(R.id.nomeEvento);
            pontosEvento = (TextView) itemView.findViewById(R.id.pontosEvento);
            valorEvento = (TextView) itemView.findViewById(R.id.valorEvento);
            enderecoEvento = (TextView) itemView.findViewById(R.id.enderecoEvento);
            detalhes = (Button) itemView.findViewById(R.id.detalhes);
        }
    }

    private List<Evento> mEventos;
    private Context context;
    private String user;
    private Long senha;

    public EventosAdapter(List<Evento> eventos, Context context, String userLogado, Long senhaLogado) {
        mEventos = eventos;
        this.context = context;
        this.user = userLogado;
        this.senha = senhaLogado;
    }

    @Override
    public EventosAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(context);

        View contactView = inflater.inflate(R.layout.item_lista_eventos, parent, false);

        ViewHolder viewHolder = new ViewHolder(contactView);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int position) {
        Evento evento = mEventos.get(position);

        TextView nomeEvento = viewHolder.nomeEvento;
        nomeEvento.setText(evento.getNome());
        TextView pontosEvento = viewHolder.pontosEvento;
        Double pontos = (evento.getValor() * 0.25);
        int pontosInt = pontos.intValue();
        pontosEvento.setText("Pontos: " + pontos.intValue());
        TextView valorEvento = viewHolder.valorEvento;
        valorEvento.setText("R$ " + String.format("%.2f", evento.getValor()).replace(".", ","));
        TextView enderecoEvento = viewHolder.enderecoEvento;
        enderecoEvento.setText(evento.getEndereco());

        Button detalhes = viewHolder.detalhes;

        detalhes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentVaiPraCadastroEvento = new Intent(context, CadastroEvento.class);
                intentVaiPraCadastroEvento.putExtra("isEdit", true);
                intentVaiPraCadastroEvento.putExtra("position", position);
                intentVaiPraCadastroEvento.putExtra("user", user);
                intentVaiPraCadastroEvento.putExtra("senha", senha);

                context.startActivity(intentVaiPraCadastroEvento);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mEventos.size();
    }
}